plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("com.android.library")
}

@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
    targetHierarchy.default()

    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "1.8"
            }
        }
    }

    js(IR) {
        moduleName = "shared"
        binaries.executable()
        browser()
        binaries.library()
        nodejs()
    }

    sourceSets {

        val ktorVersion = "2.3.3"
        val coroutinesVersion = "1.7.3"

        val commonMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
            }
        }

        val androidMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(npm("uuid", "9.0.0"))
                implementation("io.ktor:ktor-client-js:$ktorVersion")
            }
        }
    }
}

android {
    namespace = "ru.auroraos.gitviewer"
    compileSdk = 34
    defaultConfig {
        minSdk = 26
    }
}

tasks.register("jsBuildForAurora") {
    dependsOn("jsBrowserDevelopmentWebpack")
    doLast {
        copy {
            from(layout.buildDirectory.dir("dist/js/developmentExecutable"))
            into("${rootProject.rootDir}/auroraApp/GitViewer/qml/kmp")
        }
    }
}

tasks.register("jsBuildLibForReact") {
    dependsOn("jsNodeProductionLibraryDistribution")
}
