/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer

import ru.auroraos.gitviewer.extensions.sendEvent
import ru.auroraos.gitviewer.services.ServiceRequestJS

fun main() {}

/**
 * Js service HTTP request
 */
@OptIn(ExperimentalJsExport::class)
@JsExport
val ServiceRequestJS = ServiceRequestJS()

/**
 * Init fun for run after ready index.html
 */
@OptIn(ExperimentalJsExport::class)
@JsExport
fun init() = sendEvent("Init")

/**
 * NPM package for generate UUID
 */
@JsModule("uuid")
@JsNonModule
external object Uuid {
    fun v4(): String
}
