/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.extensions

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.asPromise
import kotlinx.coroutines.async
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import ru.auroraos.gitviewer.Uuid;

fun <T : Any> CoroutineScope.promiseWithEvent(
    enable: Boolean,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> T
): Any {
    return if (enable) {
        val caller = Uuid.v4()
        async(context, start, block).asPromise().then({
            sendEventResponse(caller = caller, response = it)
        }, {
            sendEventError(caller = caller, error = it.message ?: "Error query")
        })
        caller
    } else {
        async(context, start, block).asPromise()
    }
}
