/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.extensions

import kotlinx.browser.document
import org.w3c.dom.CustomEvent
import org.w3c.dom.CustomEventInit

fun sendEvent(
    caller: String,
) = _sendEvent(
    caller = caller,
    response = null,
    error = null
)

fun sendEventError(
    caller: String,
    error: String,
) = _sendEvent(
    caller = caller,
    response = null,
    error = error
)

fun <T> sendEventResponse(
    caller: String,
    response: T,
) = _sendEvent(
    caller = caller,
    response = response,
    error = null
)

@Suppress("UNUSED_PARAMETER")
private fun <T> _sendEvent(
    caller: String,
    response: T?,
    error: String?,
) {
    document.dispatchEvent(
        CustomEvent(
            type = "framescript:log",
            eventInitDict = CustomEventInit(
                detail = js("{'caller': caller, 'response': response, 'error': error}")
            ),
        )
    )
}
