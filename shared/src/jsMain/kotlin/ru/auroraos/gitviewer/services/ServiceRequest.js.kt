/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.services

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.js.Js
import ru.auroraos.gitviewer.services.impl.GetRequestJS

/**
 * Get platform JS client
 */
actual fun httpClient(config: HttpClientConfig<*>.() -> Unit) = HttpClient(Js) {
    config(this)
}

/**
 * JS service network
 */
@JsExport
@OptIn(ExperimentalJsExport::class)
class ServiceRequestJS {
    private val request = ServiceRequest()

    val get by lazy { GetRequestJS(request, false) }
    val getEvent by lazy { GetRequestJS(request, true) }
}
