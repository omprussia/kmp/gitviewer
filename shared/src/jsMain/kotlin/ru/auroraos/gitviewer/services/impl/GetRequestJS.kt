/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.services.impl

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import ru.auroraos.gitviewer.extensions.promiseWithEvent
import ru.auroraos.gitviewer.services.ServiceRequest

@JsExport
@OptIn(ExperimentalJsExport::class)
@Suppress("unused", "NON_EXPORTABLE_TYPE")
class GetRequestJS(
    private val client: ServiceRequest,
    private val isEvent: Boolean
) {
    @OptIn(DelicateCoroutinesApi::class)
    fun projects(sort: Boolean = false) = GlobalScope.promiseWithEvent(isEvent) {
        client.get.projects(sort).toTypedArray()
    }
}
