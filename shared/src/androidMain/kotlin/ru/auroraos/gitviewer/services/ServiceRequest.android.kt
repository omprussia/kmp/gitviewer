/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.services

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig

/**
 * Get platform Android client
 */
actual fun httpClient(config: HttpClientConfig<*>.() -> Unit) = HttpClient {
    config(this)
}
