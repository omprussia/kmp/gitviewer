/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.exception

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

/**
 * Error response [Exception]
 */
@OptIn(ExperimentalJsExport::class)
@JsExport
data class ResponseException(
    val code: Int,
    val error: String,
) : RuntimeException()
