/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.responses

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@OptIn(ExperimentalJsExport::class)
@JsExport
@Serializable
data class ProjectResponse(
    @SerialName("id")
    val id: String,
    @SerialName("web_url")
    val url: String,
    @SerialName("avatar_url")
    val avatar: String?,
    @SerialName("name")
    val name: String,
    @SerialName("description")
    val description: String?,
    @SerialName("created_at")
    val createdAt: String,
)
