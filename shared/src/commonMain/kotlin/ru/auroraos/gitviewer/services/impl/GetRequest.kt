/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.services.impl

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import ru.auroraos.gitviewer.responses.ProjectResponse

class GetRequest(private val client: HttpClient) {

    companion object {
        /**
         * GitLab path org
         */
        const val GROUP = "omprussia";
    }

    /**
     * Get list projects
     */
    @Throws(Exception::class)
    suspend fun projects(sort: Boolean = false): List<ProjectResponse> {
        return client.get("groups/$GROUP/projects") {
            url {
                parameters.append("include_subgroups", true.toString())
                parameters.append("sort", if (sort) "asc" else "desc")
                parameters.append("per_page", 100.toString())
            }
        }.body()
    }
}
