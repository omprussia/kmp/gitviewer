/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Refresh
import androidx.compose.material.icons.outlined.Sort
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import ru.auroraos.gitviewer.responses.ProjectResponse

class MainActivity : ComponentActivity() {

    private val viewModel: MainModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {

                val repos by viewModel.repos.collectAsState()
                val isSort by viewModel.isSort.collectAsState()

                val angle: Float by animateFloatAsState(
                    if (isSort) 180f else 0f,
                    label = "isSort"
                )

                Scaffold(
                    backgroundColor = Color(0XFFecf7f8),
                    topBar = {
                        TopAppBar(
                            title = {
                                Text("Git Viewer")
                            },
                            actions = {
                                IconButton(
                                    enabled = !repos.isNullOrEmpty(),
                                    onClick = {
                                        viewModel.refresh()
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Outlined.Refresh,
                                        contentDescription = "Refresh"
                                    )
                                }

                                IconButton(
                                    enabled = !repos.isNullOrEmpty(),
                                    onClick = {
                                        viewModel.toggleSort()
                                        viewModel.refresh()
                                    }
                                ) {
                                    Icon(
                                        modifier = Modifier.rotate(angle),
                                        imageVector = Icons.Outlined.Sort,
                                        contentDescription = "Sort"
                                    )
                                }
                            }
                        )
                    }
                ) { paddingValues ->
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(paddingValues)
                    ) {
                        when {
                            repos == null -> LoadingView()
                            repos?.isEmpty() == true -> EmptyView()
                            else -> ListView(repos!!)
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ListView(
    list: List<ProjectResponse>
) {
    val uriHandler = LocalUriHandler.current

    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        items(items = list, itemContent = { item ->
            Card(
                modifier = Modifier
                    .fillMaxWidth(),
                onClick = {
                    uriHandler.openUri(item.url)
                }
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    AsyncImage(
                        modifier = Modifier
                            .size(50.dp)
                            .clip(RoundedCornerShape(16))
                            .background(Color.LightGray),
                        model = item.avatar,
                        placeholder = painterResource(android.R.drawable.stat_notify_sync),
                        error = painterResource(android.R.drawable.ic_menu_gallery),
                        contentDescription = item.name
                    )

                    Spacer(modifier = Modifier.size(16.dp))

                    Column {
                        Text(
                            text = item.name,
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold
                        )

                        item.description?.let {
                            if (it.isNotBlank()) {
                                Spacer(modifier = Modifier.size(4.dp))
                                Text(text = it)
                            }
                        }

                        Spacer(modifier = Modifier.size(6.dp))

                        Text(
                            text = item.createdAt,
                            fontSize = 14.sp,
                            color = Color.Gray,
                        )
                    }
                }
            }
        })
    }
}

@Composable
fun LoadingView() {
    Box(modifier = Modifier.fillMaxSize()) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = "Loading..."
        )
    }
}

@Composable
fun EmptyView() {
    Box(modifier = Modifier.fillMaxSize()) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = "Empty list"
        )
    }
}
