/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.gitviewer.android

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.auroraos.gitviewer.responses.ProjectResponse
import ru.auroraos.gitviewer.services.ServiceRequest

class MainModel : ViewModel() {

    /**
     * [ServiceRequest] allows you to make requests to the network
     */
    private val query = ServiceRequest()

    /**
     * [MutableStateFlow] sort direction
     */
    private val _isSort: MutableStateFlow<Boolean> = MutableStateFlow(false)

    /**
     * [StateFlow] for [_isSort]
     */
    val isSort: StateFlow<Boolean> get() = _isSort.asStateFlow()

    /**
     * [MutableStateFlow] for start app and end splash
     */
    private val _repos: MutableStateFlow<List<ProjectResponse>?> = MutableStateFlow(null)

    /**
     * [StateFlow] for [_repos]
     */
    val repos: StateFlow<List<ProjectResponse>?> get() = _repos.asStateFlow()

    init {
        refresh()
    }

    fun toggleSort() {
        _isSort.value = !_isSort.value
    }

    fun refresh() {
        _repos.value = null
        updateRepose()
    }

    private fun updateRepose() {
        viewModelScope.launch {
            try {
                _repos.value = query.get.projects(_isSort.value)
            } catch (ex: Exception) {
                Log.e("ERROR", ex.message.toString())
            }
        }
    }
}
