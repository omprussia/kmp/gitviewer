/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: root

    property var repos: []
    property bool isSort
    property bool isError
    property bool isLoading

    function getRepos() {
        // clear data
        repos = []
        isError = false
        isLoading = true
        listRepos.clear()

        // send query
        libKMPShared.run(
            // JS method with params
            "shared.ru.auroraos.gitviewer.ServiceRequestJS.getEvent.projects(" + (isSort ? "true" : "false") + ")",

            // success response
            function(response) {
                for (var index = 0; index < response.length; index++) {
                    listRepos.append(response[index])
                }
                isLoading = false
            },

            // error response
            function(error) {
                isLoading = false
                isError = true
            }
        );
    }

    allowedOrientations: Orientation.All

    ListModel {
        id: listRepos
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Large
        running: isLoading
        visible: isLoading
    }

    Text {
        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge * 2
        text: qsTr("An unexpected error occurred, please try again later")
        visible: isError
        wrapMode: Text.WordWrap
        color: Theme.primaryColor
        textFormat: Text.StyledText
        horizontalAlignment: Qt.AlignCenter
        font.pixelSize: Theme.fontSizeLarge
    }

    SilicaListView {
        anchors.fill: parent
        spacing: Theme.paddingMedium
        model: listRepos
        delegate: DelegateRepo {
            onClicked: Qt.openUrlExternally(model.url)
        }
        header: PageHeader {
            title: qsTr("Git Viewer")
        }

        PullDownMenu {
            id: menu

            busy: isLoading
            topMargin: Theme.paddingLarge
            bottomMargin: Theme.paddingLarge

            MenuItem {
                text: qsTr("Reload")

                onClicked: root.getRepos()
            }

            MenuItem {
                text: qsTr("Sort " + (isSort ? "DESC" : "ASC"))

                onClicked: {
                    root.isSort = !root.isSort
                    root.getRepos()
                }
            }
        }

         VerticalScrollDecorator {}
    }

    Component.onCompleted: {
        root.getRepos()
    }
}
