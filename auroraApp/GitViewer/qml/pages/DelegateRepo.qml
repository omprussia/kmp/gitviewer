/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

ListItem {
    id: root

    width: parent.width
    contentHeight: content.height + Theme.paddingMedium * 2

    Row {
        id: content

        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge * 2
        spacing: Theme.paddingMedium

        Rectangle {
            id: imgData

            anchors.top: parent.top
            anchors.topMargin: Theme.dp(8)
            width: Theme.iconSizeMedium
            height: Theme.iconSizeMedium
            color: img.status == Image.Ready ? 'transparent' : 'grey'
            radius: 10

            Image {
                id: img

                width: parent.width
                height: parent.width
                source: model.avatar
                fillMode: Image.PreserveAspectCrop
                visible: img.status == Image.Ready
            }

            Text {
                anchors.centerIn: parent
                text: "?"
                color: "white"
                font.pixelSize: Theme.fontSizeLarge
                font.weight: Font.Medium
                visible: img.status != Image.Ready
            }
        }

        Column {
            id: data

            width: parent.width - imgData.width
            spacing: Theme.paddingMedium

            Text {
                width: parent.width
                text: model.name
                wrapMode: Text.WordWrap
                color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
                textFormat: Text.StyledText
                horizontalAlignment: Qt.AlignLeft
                font.pixelSize: Theme.fontSizeLarge
                font.weight: Font.Medium
            }

            Text {
                width: parent.width
                text: model.description
                color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
                visible: model.description.length !== 0
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                horizontalAlignment: Qt.AlignLeft
                font.pixelSize: Theme.fontSizeSmall
                font.italic: !model.description
            }

            Text {
                width: parent.width
                text: Qt.formatDateTime(new Date(model.createdAt), "dd MMMM yyyy")
                color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                horizontalAlignment: Qt.AlignLeft
                font.pixelSize: Theme.fontSizeExtraSmall
            }
        }
    }
}
