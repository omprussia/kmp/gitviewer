/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import { SortOutlined } from "@mui/icons-material";
import { 
  Avatar, 
  Card, 
  CardActionArea, 
  CardHeader, 
  Stack, 
  Typography, 
} from "@mui/material";

export default function CardItem(props) {

    const {
      url,
      avatar,
      name,
      desc,
      date,
    } = props
  
    return (
      <Card sx={{ width: '100%' }}>
        <CardActionArea onClick={() => {
          window.open(url, "_blank", "noreferrer");
        }}>
          <CardHeader
            sx={{ alignItems: 'flex-start' }}
            avatar={(
              <Avatar src={avatar} alt={name} variant="rounded" >
                {name[0].toUpperCase()}
              </Avatar>
            )}
            title={(
              <Typography 
                sx={{ fontWeight: 'bold' }} 
                variant={'body1'} 
                color={'text.primary'}
              >
                {name}
              </Typography>
            )}
            subheader={(
              <Stack spacing={0}>
                <Typography 
                  variant={'body2'} 
                  color={'text.primary'} 
                  sx={{ pt: '3px', pb: '3px' }}
                >
                  {desc}
                </Typography>
                <Typography 
                  variant={'caption'} 
                  color={'text.secondary'}
                >
                  {new Intl
                    .DateTimeFormat(date, {
                      year: 'numeric',
                      month: 'long',
                      day: '2-digit',
                    })
                    .format(date)}
                </Typography>
              </Stack>
            )}
          />
        </CardActionArea>
      </Card>
    );
}
