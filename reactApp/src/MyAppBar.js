/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import { RefreshOutlined, SortOutlined } from "@mui/icons-material";
import { 
  Typography, 
  Box,
  IconButton,
  Toolbar,
  AppBar
} from "@mui/material";

export default function MyAppBar(props) {

    const {
      isSort,
      isLoading,
      onSort,
      onUpdate
    } = props
  
    return (
      <Box sx={{ 
          flexGrow: 1,
          overflow: 'hidden',
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10
        }}
      >
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Git Viewer
            </Typography>
            <IconButton
                disabled={isLoading}
                onClick={onUpdate}
                color="inherit"
              >
              <RefreshOutlined />
            </IconButton>
            <IconButton
                sx={{
                  transform: isSort ? 'rotate(180deg)' : 'rotate(0deg)',
                  transitionProperty: 'transform',
                  transitionTimingFunction: 'ease-in-out',
                  transitionDuration: '300ms',
                }}
                disabled={isLoading}
                onClick={onSort}
                color="inherit"
              >
                <SortOutlined />
              </IconButton>
          </Toolbar>
        </AppBar>
  
      </Box>
    );
}
