/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import * as React from 'react';
import dayjs from "dayjs";
import shared from "shared";
import { useEffect } from "react";
import { 
  Container, 
  Stack, 
  CircularProgress,
  Box,
} from "@mui/material";
import MyAppBar from "./MyAppBar";
import CardItem from "./CardItem";

/**
 * Ktor client
 */
const HttpClient = shared.ru.auroraos.gitviewer.ServiceRequestJS

export default function App() {

  const [list, setList] = React.useState([]);
  const [sort, setSort] = React.useState(false);
  const [toggle, setToggle] = React.useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setList(await HttpClient.get.projects(sort));
    }
    fetchData().catch(console.error);
  }, [toggle]);

  const items = []

  list.forEach((item) => {
    items.push((
      <CardItem
        key={`item-${item.id}`}
        url={item.url}
        avatar={item.avatar}
        name={item.name}
        desc={item.description}
        date={dayjs(item.createdAt).toDate()}
      />
    ));
  })

  return (
    <Box sx={{
      background: '#ecf7f8'
    }}>
      <Container>
        <MyAppBar
          isSort={sort}
          isLoading={list.length == 0}
          onSort={() => {
            setList([]);
            setSort(!sort);
            setToggle(!toggle);
          }}
          onUpdate={() => {
            setList([]);
            setToggle(!toggle);
          }}
        />
        {list.length == 0 ? (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            minHeight="100vh"
          >
            <CircularProgress />
          </Box>
        ) : (
          <Stack
            spacing={2}
            sx={{ 
              pt: 2, 
              pb: 2 
            }}
          >
            {items}
          </Stack>
        )}
      </Container>
    </Box>
  );
}
