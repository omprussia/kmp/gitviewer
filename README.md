# Git Viewer

<p align="center">
  <img src="data/docs/preview.png" />
</p>

Kotlin Multiplatform приложение демонстрирующее работу общего модуля KMP на 3 платформах: Android, Web, Aurora OS. Приложения выполняют запрос в GitLab REST API на получение проектов и выводит результат через общий модуль KMP.

В этом приложении мы не использовали отдельный компонент Wrapper (подробнее о связующем компоненте можно узнать в статье [Kotlin Multiplatform в ОС Аврора](https://habr.com/ru/articles/753570/)) все необходимое реализовано в KMP модуле.

## Структура проекта

Этот репозиторий содержит общий мультиплатформенный модуль Kotlin, проекты Android, Web, ОС Аврора. Общий модуль связан с проектами через механизм мультиплатформенности Gradle. Для использования в приложении Web и ОС Аврора общий модуль компилируется в JS библиотеки через задачи внутренней интеграции Gradle. JS библиотека в формате npm пакета подключается к проекту Web. JS библиотека собранная как исполняемая, подключается к ОС Аврора через компонент основанный на WebView.

<p align="center">
  <img src="data/docs/structure.jpg" />
</p>

## UI

Все приложения имеют нативный UI интерфейс для своей платформы. Jetpack Compose для Android, Qt/QML в ОС Аврора, MUI с React.js для web browser. Kotlin Multiplatform дополняет бизнес логику приложений.

## Сборка

Со сборкой Android все стандартно - сборка приложения и JVM модуля. Для Web приложения на React собирается JS модуль как `library`, а для ОС Аврора JS модуль как `executable`. Команды на сборку следующие:

```shell
# Android
./gradlew :androidApp:assemble

# Web React
./gradlew :shared:jsBuildLibForReact

# Aurora OS
./gradlew :shared:jsBuildForAurora
```
